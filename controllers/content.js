const Model = require('../models');

const { Module } = Model;

const contentController = {
  // returns all the modules
  all(req, res) {
    Module.find({}, (err, modules) => {
      if (err) {
        return (err);
      }
      res.json(modules);
    });
  },
  // returns a module with it's properties
  byModuleName(req, res) {
    const aliasParam = req.params.aliasParam;

    Module.findOne({ alias: aliasParam }, (err, module) => {
      if (err) {
        return (err);
      }
      console.log(module);
      res.json(module);
    });
  },

  byId(req, res) {
    const idParam = req.params.id;

    Module.findOne({ _id: idParam }, (err, module) => {
      if (err) {
        return (err);
      }
      res.json(module);
    });
  },
};


module.exports = contentController;
