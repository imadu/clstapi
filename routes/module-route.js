const express = require('express');

const router = express.Router();
const contentController = require('../controllers/content');

router.get('/', contentController.all);
router.get('/:aliasParam', contentController.byModuleName);
router.get('/:id', contentController.byId);
module.exports = router;
