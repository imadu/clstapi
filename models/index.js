const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const ModuleSchema = new mongoose.Schema({
  id: { type: String, default: new ObjectId(), required: true },
  title: { type: String, required: true },
  alias: {
    type: String, lowercase: true, unique: true, required: true
  },
  description: { type: String },
  status: { type: Boolean, default: true },
  properties: { type: Array, default: [] },
  created: { by: { type: String }, time: { type: Date, default: new Date() } },
});

const Module = mongoose.model('Module', ModuleSchema);

module.exports = { Module };
